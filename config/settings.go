package config

const (
	Goroutines    = 3 // кол-во воркеров пишущих в кассандру
	Kafkaroutines = 1
	Keyspace      = "testspace"
	Node          = "46.4.16.136" // TODO: Fix this.
	KafkaNode     = "78.46.107.197:9092"
	Loglevel      = "info"
	KafkaTopic    = "pashtettopic"
	Logfile       = "handler_cassa.log"
	ToKafka       = true
	ToCassandra   = true
)
