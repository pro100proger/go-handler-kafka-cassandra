package handler

import (
	"../config"
	"../db"
	"../logger"
	"./datas"
	"encoding/json"
	"github.com/bitly/go-simplejson"
	"io/ioutil"
	"net/http"
	"time"
)

func STC_Handler(w http.ResponseWriter, r *http.Request) {
	bytearr, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.LogError("Cannot decode stream " + err.Error() + "\n" + string(bytearr))
	}

	jsonmap, err := simplejson.NewJson(bytearr)
	if err != nil {
		logger.LogError("Cannot decode stream " + err.Error() + "\n" + string(bytearr))
	}

	res2D := &datas.Response{
		Result: true,
		Data:   []string{""},
	}

	js, err := json.Marshal(res2D)
	if err != nil {
		logger.LogError("Error dump into json obj")
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

	fs, _ := (*jsonmap).Array()

	t := time.Now()
	for _, values := range fs {
		// fmt.Println(values)
		as, err := json.Marshal(values)
		if err != nil {
			logger.LogError("Error dump into json obj")
		}

		if config.ToCassandra {
			db.Binsert(string(as), t.Format(time.RFC850), t.Format(time.RFC850), r.RemoteAddr, "dshbj-sk12d-dfb25-jderr")
		}

		if config.ToKafka {
			go db.Kinsert(string(as))
		}
	}
	/*
		Для декодирования JSON перевел полученный поток байтов в
		ридер и декодировал + добавил интерфейсы к кривым данным
	*/
	/*
		Новая переменная нужна для получения всех имеющихся значений
		Если сделать ссылку работать не будет
		Короче new нужно
	*/
	/*
		Декодировать нужно именно принятый поток байт,
		если попытаться декодировать поток байт из соединения,
		произойдет ошибка
	*/
}
