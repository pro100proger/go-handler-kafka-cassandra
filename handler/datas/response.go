package datas

// Структура Ответа от сервера в формате JSON
type Response struct {
	Result bool     `json:"result"`
	Data   []string `json:"data"`
}
