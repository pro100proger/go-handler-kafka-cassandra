package datas

// Структура Ответа от сервера в формате JSON
type Cassa struct {
	Result bool     `json:"result"`
	Data   []string `json:"data"`
}
