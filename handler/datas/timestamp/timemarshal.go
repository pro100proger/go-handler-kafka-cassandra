package timestamp

import (
	"../../../logger"
	"fmt"
	"strconv"
	"time"
)

type Timestamp struct {
	time.Time
}

func (t *Timestamp) MarshalJSON() ([]byte, error) {
	ts := t.Time.Unix()
	stamp := fmt.Sprint(ts)

	return []byte(stamp), nil
}

func (t *Timestamp) UnmarshalJSON(b []byte) error {
	slice := b[0 : len(b)-2]
	ts, err := strconv.Atoi(string(slice))
	if err != nil {
		logger.LogError(err.Error())
	}

	t.Time = time.Unix(int64(ts), 0)

	return nil
}

// func (t Timestamp) GetBSON() (interface{}, error) {
// 	if time.Time(*t).IsZero() {
// 		return nil, nil
// 	}

// 	return time.Time(*t), nil
// }

// func (t *Timestamp) SetBSON(raw bson.Raw) error {
// 	var tm time.Time

// 	if err := raw.Unmarshal(&tm); err != nil {
// 		return err
// 	}

// 	*t = Timestamp(tm)

// 	return nil
// }

// func (t *Timestamp) String() string {
// 	return time.Time(*t).String()
// }
