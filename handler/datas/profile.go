package datas

import (
	"./timestamp"
)

type DataProf struct {
	Manufakture string `json:manufakture`
	Model       string `json:model`
	UserAgent   string `json:user_agent`
}

type Profile struct {
	// clientid  string `json:"client_id"`
	Innerid string              `json:"inner_id"`
	Times   timestamp.Timestamp `json:"timestamp"`
	Data    DataProf            `json:"data"`
	Event   string              `json:"uid"`
}

type JSONProf map[int]Profile
