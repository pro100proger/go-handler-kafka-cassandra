package db

import (
	"../config"
	"../logger"
	"github.com/Shopify/sarama"
)

var (
	// kafkaStream chan *sarama.ProducerMessage
	sarProd sarama.AsyncProducer
)

func Kinsert(js string) {
	// Послать в пачку и начать писать в новую
	sarProd.Input() <- &sarama.ProducerMessage{Topic: config.KafkaTopic, Value: sarama.StringEncoder(js)}
}

func Initkafka() {
	// kafkaStream = make(chan *sarama.ProducerMessage, 0)

	ses, err := sarama.NewAsyncProducer([]string{config.KafkaNode}, nil)
	if err != nil {
		logger.LogError(err.Error())
	}
	sarProd = ses
}
