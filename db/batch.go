package db

import (
	"../config"
	"../logger"
	"github.com/gocql/gocql"
	"time"
)

const query_string = "INSERT INTO test_text (timestamp_header, timestamp_server, ip, uid, data, dumped) VALUES (?, ?, ?, ?, ?, ?)"

var (
	in      chan *gocql.Batch
	counter int
	b       *gocql.Batch
)

// функция для ассинхронной работы с Cassandra
func Binsert(js string, stime string, htime string, IP string, Uid string) {
	// Собираем пачки значений перед записью
	counter++
	b.Query(query_string, string(htime), time.Now().Format(time.RFC850), IP,
		Uid, string(js), 0)
	if counter%50 == 0 {
		// Послать в пачку и начать писать в новую
		in <- b
		b = gocql.NewBatch(gocql.LoggedBatch)
		counter = 0
	}
}

func Init() {
	// Подключаемся к базе
	cluster := gocql.NewCluster(config.Node)
	cluster.Keyspace = config.Keyspace
	cluster.Consistency = gocql.Any
	counter = 0
	in = make(chan *gocql.Batch, 0)

	// Начинаем новую сессию
	session, err := cluster.CreateSession()
	if err != nil {
		logger.LogError("Then starting new session on " + config.Node + ":\n" + err.Error())
	} else {
		logger.LogInfo("Start new session on " + config.Node)
	}
	// Ассинхронность
	for i := 0; i < config.Goroutines; i++ {
		go processBatches(session, in)
	}

	b = session.NewBatch(gocql.LoggedBatch)
}

func processBatches(s *gocql.Session, in chan *gocql.Batch) {
	for {
		batch := <-in
		for {
			if err := s.ExecuteBatch(batch); err != nil {
				logger.LogError("Cannot execute batch: " + err.Error())
				continue // Ошибки записываем и продолжаем писать в базу
			} else {
				// logger.LogInfo("Batch executetd")
				break
			}
		}
	}
}
