package main

import (
	"./config"
	"./db"
	"./handler"
	"./logger"
	"log"
	"net/http"
	"os"
)

/*
	Код стабильно обрабатывает 12К запросов в секунду, при этом кода тут кот наплакал
	Проверял штатным тестером apache benchmark:

	ab -q -c 50 -n 1000 http://127.0.0.1:8083/ | grep "Requests per second"

	Сервер размазывает нагрузку равномерно
*/
var (
	logFp *os.File
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Usage: main <port>")
	}
	var err error
	logFile := config.Logfile

	if logFp, err = os.OpenFile(logFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666); err != nil {
		log.Fatal("Cannot open log file: ", err.Error())
	}

	logger.Init(logFp, logFp, logFp, logFp)
	http.HandleFunc("/", handler.STC_Handler)

	logger.LogInfo("Server start on port " + os.Args[1])

	if config.ToCassandra {
		db.Init()
	}

	if config.ToKafka {
		db.Initkafka()
	}

	http.ListenAndServe(":"+os.Args[1], nil)
	// Использует несколько потоков для распределения нагрузки
}
